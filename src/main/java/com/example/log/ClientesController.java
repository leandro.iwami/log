package com.example.log;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@RestController
public class ClientesController {

	Logger logger = LoggerFactory.getLogger(ClientesController.class);
	
	@RequestMapping("/logs")
	public String index() {
		logger.trace("A Trace message");
		logger.debug("A DEBUG message");
		logger.info("A Info message");
		logger.warn("A warn message");
		logger.error("A Error message");
		return "Leia o log Motherfucker!!!!!!!";
	}
	
	@RequestMapping("/logs-aprovado")
	public String aprovado() {
		logger.info("cartão Aprovado");
		return "Aprovado!!!!";
	}
	
	@RequestMapping("/logs-reprovado")
	public String reprovado() {
		logger.info("cartão Reprovado");
		return "Reprovado Otario!!!!";
	}
	
}
